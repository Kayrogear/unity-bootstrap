﻿using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

[InitializeOnLoad]
public class BootstrapSettingsWindow : EditorWindow
{
    static BootstrapSettingsWindow()
	{
		EditorApplication.update += OnEditorUpdate;
	}

	private static void OnEditorUpdate()
	{
		EditorApplication.update -= OnEditorUpdate;
		LoadSettings();
	}

	[MenuItem("Window/Bootstrap Settings")]
    public static void Open()
    {
        var window = GetWindow<BootstrapSettingsWindow>();
		window.minSize = new Vector2(330f, 360f);
		window.maxSize = new Vector2(600f, 4000f);
		window.titleContent = new GUIContent("Bootstrap Settings");
		window.Show();
    }

	private static void LoadSettings()
	{
		var startScenePath = BootstrapSettings.Config.StartSceneName;
		if (!string.IsNullOrEmpty(startScenePath))
		{
			var sceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(startScenePath);
			if (sceneAsset == null)
				Debug.LogWarning("Starts scene [" + startScenePath + "] path not found!");
			else
				EditorSceneManager.playModeStartScene = sceneAsset;
		}
		else
		{
			//Open();
		}
	}

    private void OnEnable()
    {
        LoadSettings();
    }

	private void OnGUI()
	{
		// Preloader Scene
		//----------------
		{
			string beforeScenePath = "";
			if (EditorSceneManager.playModeStartScene != null)
			{
				beforeScenePath = AssetDatabase.GetAssetPath(EditorSceneManager.playModeStartScene);
			}

			EditorGUILayout.Space();
			EditorGUILayout.LabelField("Select preloader scene: ");
			EditorSceneManager.playModeStartScene =
				(SceneAsset) EditorGUILayout.ObjectField(EditorSceneManager.playModeStartScene, typeof(SceneAsset),
					false);

			string afterScenePath = "";
			if (EditorSceneManager.playModeStartScene != null)
			{
				afterScenePath = AssetDatabase.GetAssetPath(EditorSceneManager.playModeStartScene);
			}

			if (beforeScenePath != afterScenePath)
			{
				BootstrapSettings.Config.StartSceneName = afterScenePath;
				BootstrapSettings.SaveConfig();
			}
		}

		EditorGUILayout.Space();

		GUILayout.BeginVertical("Box");
		{
			if (string.IsNullOrEmpty(BootstrapSettings.Config.PathToGitExecutable))
				EditorGUILayout.LabelField($"Select Path to Git");
			else
			{
				EditorGUILayout.LabelField($"Path to Git:");
				EditorGUILayout.LabelField(BootstrapSettings.Config.PathToGitExecutable);
			}

			if (GUILayout.Button("Select Path"))
			{

				// Path To Git
				//------------
				string gitExtension = "";
				switch (Application.platform)
				{
					case RuntimePlatform.WindowsEditor:
						gitExtension = "exe";
						break;
					case RuntimePlatform.OSXEditor:
						gitExtension = "";
						break;
				}

				var path = EditorUtility.OpenFilePanel("Select Git", BootstrapSettings.Config.PathToGitExecutable,
					gitExtension);

				if (File.Exists(path))
				{
					BootstrapSettings.Config.PathToGitExecutable = path;
					BootstrapSettings.SaveConfig();
				}
			}
		}
		GUILayout.EndVertical();
	}
}