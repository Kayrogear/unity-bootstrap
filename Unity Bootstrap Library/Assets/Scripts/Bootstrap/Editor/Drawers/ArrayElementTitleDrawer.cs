﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(ArrayElementTitleAttribute))]
public class ArrayElementTitleDrawer : PropertyDrawer
{
	protected virtual ArrayElementTitleAttribute Attribute => (ArrayElementTitleAttribute)attribute;

	private readonly List<string> _labelsList = new List<string>();

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return EditorGUI.GetPropertyHeight(property, label, true);
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		_labelsList.Clear();
		foreach (var varName in Attribute.VarNames)
		{
			var fullPathName = property.propertyPath + "." + varName;
			var titleNameProp = property.serializedObject.FindProperty(fullPathName);
			_labelsList.Add(GetTitle(titleNameProp));
		}

		var separator = string.IsNullOrEmpty(Attribute.Separator) ? "" : Attribute.Separator;
		var labelText = string.Join(separator, _labelsList);

		if (string.IsNullOrEmpty(labelText))
			labelText = label.text;
		
		EditorGUI.PropertyField(position, property, new GUIContent(labelText, label.tooltip), true);
	}

	private string GetTitle(SerializedProperty property)
	{
		switch (property.propertyType)
		{
			case SerializedPropertyType.Generic:

				if (property.isArray)
				{
					var stringsList = new List<string>();
					for (int i = 0; i < property.arraySize; i++)
					{
						var element = property.GetArrayElementAtIndex(i);
						var title = GetTitle(element);
						if (string.IsNullOrEmpty(title)) continue;
						stringsList.Add(title);
					}

					return string.Join("_", stringsList);
				}

				break;
			case SerializedPropertyType.Integer:
				return property.intValue.ToString();
			case SerializedPropertyType.Boolean:
				return property.boolValue.ToString();
			case SerializedPropertyType.Float:
				return property.floatValue.ToString();
			case SerializedPropertyType.String:
				return property.stringValue;
			case SerializedPropertyType.Color:
				return property.colorValue.ToString();
			case SerializedPropertyType.ObjectReference:
				return property.objectReferenceValue.ToString();
			case SerializedPropertyType.LayerMask:
				break;
			case SerializedPropertyType.Enum:

				var fieldInfo = GetFieldOfProperty(property);
				
				if (property.enumValueIndex < 0)
				{
					//var flags = property.intValue;
					//var list = MaskToList<Enum>(targetEnum);
					//var sb = new StringBuilder();
					//foreach (var @enum in list)
					//{
					//	sb.Append(@enum.ToString());
					//}

					//return sb.ToString();
					
				}
				else
				{
					return property.enumValueIndex < property.enumNames.Length &&
						   property.enumValueIndex >= 0
						? property.enumNames[property.enumValueIndex]
						: "[none]";
				}
				break;
				

			case SerializedPropertyType.Vector2:
				return property.vector2Value.ToString();
			case SerializedPropertyType.Vector3:
				return property.vector3Value.ToString();
			case SerializedPropertyType.Vector4:
				return property.vector4Value.ToString();
			case SerializedPropertyType.Rect:
				break;
			case SerializedPropertyType.ArraySize:
				break;
			case SerializedPropertyType.Character:
				break;
			case SerializedPropertyType.AnimationCurve:
				break;
			case SerializedPropertyType.Bounds:
				break;
			case SerializedPropertyType.Gradient:
				break;
			case SerializedPropertyType.Quaternion:
				break;
			default:
				break;
		}
		return "";
	}

	private FieldInfo GetFieldOfProperty(SerializedProperty prop)
	{
		if (prop == null) return null;

		var tp = GetTargetType(prop.serializedObject);
		if (tp == null) return null;

		var path = prop.propertyPath.Replace(".Array.data[", "[");
		var elements = path.Split('.');
		System.Reflection.FieldInfo field = null;

		foreach (var element in elements)
		{
			if (element.Contains("["))
			{
				var elementName = element.Substring(0, element.IndexOf("["));
				var index = System.Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));

				field = tp.GetMember(elementName, MemberTypes.Field, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).FirstOrDefault() as System.Reflection.FieldInfo;
				if (field == null) return null;
				tp = field.FieldType;
			}
			else
			{
				field = tp.GetMember(element, MemberTypes.Field, BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).FirstOrDefault() as System.Reflection.FieldInfo;
				if (field == null) return null;
				tp = field.FieldType;
			}
		}
		return field;
	}

    private Type GetTargetType(SerializedObject obj)
    {
        if (obj == null) return null;

        if(obj.isEditingMultipleObjects)
        {
            var c = obj.targetObjects[0];
            return c.GetType();
        }
        else
        {
            return obj.targetObject.GetType();
        }
    }

	public static IEnumerable<T> MaskToList<T>(Enum mask)
	{
		if (typeof(T).IsSubclassOf(typeof(Enum)) == false)
			throw new ArgumentException();

		return Enum.GetValues(typeof(T))
			.Cast<Enum>()
			.Where(m => mask.HasFlag(m))
			.Cast<T>();
	}
}