﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;


public static class EditorTools
{
	public static bool IsContainsDefine(string definitionName, IEnumerable<BuildTargetGroup> buildTargets)
	{
		var flag = false;
		foreach (var buildTargetGroup in buildTargets)
		{
			var defineSymbolsStr = PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup);	
			var symbols = new List<string>(defineSymbolsStr.Split(';'));

			if (symbols.Contains(definitionName))
				return true;
		}
		return false;
	}

	public static void SetDefineEnabled(string definitionName, bool enabled, IEnumerable<BuildTargetGroup> buildTargets)
	{
		foreach (var buildTargetGroup in buildTargets)
		{
			var defineSymbolsStr = PlayerSettings.GetScriptingDefineSymbolsForGroup(buildTargetGroup);	
			var symbols = new List<string>(defineSymbolsStr.Split(';'));

			if (enabled)
			{
				symbols.Remove(definitionName);
				symbols.Add(definitionName);
			}
			else
			{
				symbols.Remove(definitionName);	
			}
			
			PlayerSettings.SetScriptingDefineSymbolsForGroup(buildTargetGroup, string.Join(";", symbols.ToArray()));
		}
	}

	public static int GetBuildNumber()
	{
		int iosBuildNumber;
		int.TryParse(PlayerSettings.iOS.buildNumber, out iosBuildNumber);
		var androindBundleVerCode = PlayerSettings.Android.bundleVersionCode;
		return Mathf.Max(iosBuildNumber, androindBundleVerCode);
	}

	public static string GetCurrentGitRevision()
	{
		var branch = "";
		var rev = "";

		if (string.IsNullOrEmpty(BootstrapSettings.Config.PathToGitExecutable))
			return rev;
		
		var result = false;
		try
		{
			var p = new Process();

			p.StartInfo.FileName = BootstrapSettings.Config.PathToGitExecutable;
			p.StartInfo.UseShellExecute = false;
			p.StartInfo.RedirectStandardOutput = true;
			p.StartInfo.RedirectStandardError = true;
			p.StartInfo.WorkingDirectory = Application.dataPath;
			p.StartInfo.CreateNoWindow = true;

			p.StartInfo.Arguments = "rev-parse HEAD";
			if (p.Start())
			{
				using (var reader = p.StandardOutput)
					rev = reader.ReadToEnd().Trim();
             
				p.WaitForExit();

				if (p.ExitCode != 0)
				{
					Debug.Log("err :" + p.StandardError.ReadToEnd());
					return null;// result;
				}
			}

			result = true;
		}
		catch (Exception e)
		{
			Debug.LogException(e);
		}

		return rev;
	}
}