﻿using System;
using System.Globalization;
using Google.Apis.Sheets.v4.Data;
using UnityEngine;

public class Spreadsheet
{
	public int RowsCount => _valueRange.Values.Count;

	private readonly ValueRange _valueRange;

	public Spreadsheet(ValueRange valueRange)
	{
		_valueRange = valueRange;
		var values = _valueRange.Values;

		if (values == null || values.Count == 0) return;

		
	}

	public int GetColumnsCount(int row)
	{
		if (row >= _valueRange.Values.Count)
			return 0;

		var columns = _valueRange.Values[row];
		return columns?.Count ?? 0;
	}

	public bool IsEmpty(int row, int column)
	{
		if (row >= _valueRange.Values.Count)
			return true;

		var columns = _valueRange.Values[row];

		if (columns == null || column >= columns.Count)
			return true;

		var value = columns[column];

		if (value is string str && string.IsNullOrEmpty(str))
			return true;

		return value == null;
	}

	public T GetValue<T>(int row, int column)
	{
		if (row >= _valueRange.Values.Count)
			return default(T);

		var columns = _valueRange.Values[row];

		if (columns == null || column >= columns.Count)
			return default(T);

		var value = columns[column];
		return GetValueInternal<T>(value);
	}

	private T GetValueInternal<T>(object value)
	{
		if (value is string && string.IsNullOrEmpty((string)value))
			return default(T);
		
		T result;
		try
		{
			result = (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
		}
		catch(Exception e)
		{
			result = default(T);
		}

		return result;
	}
}