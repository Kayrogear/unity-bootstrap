﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Util.Store;
using UnityEditor;
using UnityEngine;

public class SpreadsheetsManager
{
	private static string[] Scopes = { SheetsService.Scope.SpreadsheetsReadonly };

	private GoogleClientSecrets _secrets;
	private UserCredential _credential;
	private SheetsService _service;

	private string _appName;

	public SpreadsheetsManager(string appName, string credentialsAssetPath)
	{
		_appName = appName;

		var jsonAsset = AssetDatabase.LoadAssetAtPath<TextAsset>(credentialsAssetPath);
		//Debug.Log(jsonAsset);

		using (var stream = new MemoryStream(jsonAsset.bytes))
			_secrets = GoogleClientSecrets.Load(stream);
	}

	public async Task<Spreadsheet> LoadSpreadsheet(string id, string range, CancellationToken ct)
	{
		var service = await GetServiceAsync();
		var request = service.Spreadsheets.Values.Get(id, range);
		var response = await request.ExecuteAsync(ct);
		return new Spreadsheet(response);
	}

	private async Task<SheetsService> GetServiceAsync()
	{
		if (_service != null)
			return _service;

		Debug.Log("Get google service");

		if (_credential == null)
		{
			Debug.Log("Get google credentials");
			_credential = await GetCredentialsAsync();
		}

		// Create Google Sheets API service.
		var service = new SheetsService(new BaseClientService.Initializer()
		{
			HttpClientInitializer = _credential,
			ApplicationName = _appName,
		});

		_service = service;

		return service;
	}

	private async Task<UserCredential> GetCredentialsAsync()
	{
		// The file token.json stores the user's access and refresh tokens, and is created
		// automatically when the authorization flow completes for the first time.
		var credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
			_secrets.Secrets,
			Scopes,
			"user",
			CancellationToken.None,
			new FileDataStore(BootstrapSettings.ConfigStorePath, true));

		Debug.Log("Credential file saved to: " + BootstrapSettings.ConfigStorePath);

		return credential;
	}
}