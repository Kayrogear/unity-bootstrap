﻿using System.IO;
using UnityEditor;
using UnityEngine;

public static class ScriptableObjectTools
{
	[MenuItem("CONTEXT/ScriptableObject/Save to JSON")]
	private static void SaveToJson(MenuCommand command)
	{
		var scriptableObject = (ScriptableObject) command.context;
		var path = AssetDatabase.GetAssetPath(scriptableObject);

		var json = JsonUtility.ToJson(scriptableObject, true);

		var pathToScriptableObject = Path.GetDirectoryName(path);
		var jsonFileName = Path.Combine(pathToScriptableObject, scriptableObject.name + ".json");

		Debug.Log("Save To " + jsonFileName);
		
		File.WriteAllText(jsonFileName, json);
		AssetDatabase.Refresh();
	}

	[MenuItem("CONTEXT/ScriptableObject/Load from JSON")]
	private static void LoadFromJson(MenuCommand command)
	{
		string path = EditorUtility.OpenFilePanel("Overwrite with json", "", "json");
		if (path.Length != 0)
		{
			var jsonString = File.ReadAllText(path);

			JsonUtility.FromJsonOverwrite(jsonString, command.context);

			//if (scriptableObject == null)
			//{
			//	Debug.LogError("Scriptable Object is null!");
			//	return;
			//}

			//var pathToAsset = AssetDatabase.GetAssetPath(command.context);
			//AssetDatabase.DeleteAsset(pathToAsset);
			//AssetDatabase.CreateAsset(scriptableObject, pathToAsset);
			//AssetDatabase.Refresh();
		}
	}
}