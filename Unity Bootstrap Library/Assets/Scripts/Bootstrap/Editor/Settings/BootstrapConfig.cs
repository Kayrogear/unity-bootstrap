﻿using System;
using Newtonsoft.Json;

[JsonObject]
[Serializable]
public class BootstrapConfig
{
	public string StartSceneName { get; set; }
	public string PathToGitExecutable { get; set; }
}