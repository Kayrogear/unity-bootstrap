﻿using System;
using System.IO;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public static class BootstrapSettings
{
	public const string ConfigStorePath = ".bootstrap_config";
	public const string ConfigFileName = "config.json";

	public static BootstrapConfig Config => _config;
	public static string ConfigPath =>Path.Combine(ConfigStorePath, ConfigFileName);
	
	private static BootstrapConfig _config;
	private static JsonSerializerSettings _jsonSettings;

	static BootstrapSettings()
	{
		Debug.Log("--- Load Bootstrap Settings ---");

		if (!Directory.Exists(ConfigStorePath))
		{
			Directory.CreateDirectory(ConfigStorePath);
			
			var directoryInfo = new DirectoryInfo(ConfigStorePath);
			if ((directoryInfo.Attributes & FileAttributes.Hidden) != FileAttributes.Hidden)
				directoryInfo.Attributes |= FileAttributes.Hidden;    
		}

		_jsonSettings = new JsonSerializerSettings
		{
			Formatting = Formatting.Indented,
			MissingMemberHandling = MissingMemberHandling.Ignore
		};

		_config = LoadConfig();
	}

	public static void SaveConfig()
	{
		var jsonString = JsonConvert.SerializeObject(_config, _jsonSettings);
		File.WriteAllText(ConfigPath, jsonString);
	}

	//public static void ApplyConfig(BootstrapConfig config)
	//{
	//	_config = config;
	//}

	private static BootstrapConfig LoadConfig()
	{
		string jsonString = null;
		if (File.Exists(ConfigPath))
			jsonString = File.ReadAllText(ConfigPath);

		try
		{
			if (!string.IsNullOrEmpty(jsonString))
				return JsonConvert.DeserializeObject<BootstrapConfig>(jsonString);
		}
		catch (Exception e)
		{
			Debug.LogException(e);
		}
		
		return new BootstrapConfig();
	}

}