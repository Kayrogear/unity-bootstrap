﻿using UnityEngine;

public class ArrayElementTitleAttribute : PropertyAttribute
{
	public string[] VarNames;
	public string Separator;

	public ArrayElementTitleAttribute(string varName)
	{
		VarNames = new[] {varName};
	}

	public ArrayElementTitleAttribute(string separator, params string[] varNames)
	{
		Separator = separator;
		VarNames = varNames;
	}
}