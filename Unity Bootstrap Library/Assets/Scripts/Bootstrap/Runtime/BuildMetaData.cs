﻿using UnityEngine;

[CreateAssetMenu(fileName = "BuildMetaData", menuName = "Bootstrap/BuildMetaData")]
public class BuildMetaData : ScriptableSingleton<BuildMetaData>
{
	public string BuildVersion => _buildVersion;
	public string BuildNumber => _buildNumber;
	public string BuildDate => _buildDate;
	public string BuildRevision => _buildRev;
	
	[SerializeField] [ReadOnly] private string _buildVersion;
	[SerializeField] [ReadOnly] private string _buildNumber;
	[SerializeField] [ReadOnly] private string _buildDate;
	[SerializeField] [ReadOnly] private string _buildRev;

#if UNITY_EDITOR
	public void SetData(string version, string buildNumber, string buildDate, string buildRev)
	{
		_buildVersion = version;
		_buildNumber = buildNumber;
		_buildDate = buildDate;
		_buildRev = buildRev;
	}
#endif
}