﻿
public interface IDataProtector
{
	string Protect(string input);
	string Unprotect(string input);
}