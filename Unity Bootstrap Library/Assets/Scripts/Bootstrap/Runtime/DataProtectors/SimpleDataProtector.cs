﻿using System;
using System.Text;
using UnityEngine;

public class SimpleDataProtector : IDataProtector
{
	public const int Key = 321;

	public string Protect(string input)
	{
		//Debug.LogFormat("Protect input: {0}", input);

		var converted = Process(input, Key);
		var bytes = Encoding.UTF8.GetBytes(converted);
		var output = Convert.ToBase64String(bytes);

		//Debug.LogFormat("Protect output: {0}", output);
		return output;
	}

	public string Unprotect(string input)
	{
		try
		{
			//Debug.LogFormat("Unprotect input: {0}", input);

			var bytes = Convert.FromBase64String(input);
			var decoded = Encoding.UTF8.GetString(bytes);
			var output = Process(decoded, Key);

			//Debug.LogFormat("Unprotect output: {0}", output);

			return output;
		}
		catch (Exception e)
		{
			Debug.LogException(e);
			return null;
		}
	}

	private string Process(string input, int key)
	{
		var sb = new StringBuilder();
		foreach (var c in input)
		{
			var v = Convert.ToInt32(c);
			v ^= key;
			sb.Append(char.ConvertFromUtf32(v));
		}
		return sb.ToString();
	}
}