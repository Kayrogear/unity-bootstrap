﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Dispatcher : MonoSingleton<Dispatcher>
{
	private readonly List<Action> _invokeActions = new List<Action>();
	private readonly List<Action> _tickActions = new List<Action>();

	public void Invoke(Action action)
	{
		lock (_invokeActions)
			_invokeActions.Add(action);
	}

	public void StartTick(Action action)
	{
		lock (_tickActions)
			if (!_tickActions.Contains(action))
				_tickActions.Add(action);
	}

	public void StopTick(Action action)
	{
		lock (_tickActions)
			_tickActions.RemoveAll(a => a == action);
	}

	private void Update()
	{
		UpdateInvokeActions();
		UpdateTickActions();
	}

	private void UpdateInvokeActions()
	{
		Action[] actionsArray = null;
		lock (_invokeActions)
		{
			actionsArray = _invokeActions.ToArray();
			_invokeActions.Clear();
		}

		if (actionsArray.Any())
		{
			foreach (var action in actionsArray)
			{
				try { action(); }
				catch (Exception e) { Debug.LogException(e); }
			}
		}
	}

	private void UpdateTickActions()
	{
		Action[] actionsArray = null;
		lock (_tickActions)
			actionsArray = _tickActions.ToArray();

		if (actionsArray.Any())
		{
			foreach (var action in actionsArray)
			{
				try { action(); }
				catch (Exception e) { Debug.LogException(e); }
			}
		}
	}
}