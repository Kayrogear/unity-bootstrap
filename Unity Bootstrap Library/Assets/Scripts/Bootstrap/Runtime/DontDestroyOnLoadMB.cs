﻿using UnityEngine;

public class DontDestroyOnLoadMB : MonoBehaviour
{
	private void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}
}