﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public interface IListenersCollection
{
	void Subscribe(object listener);
	void Unsubscribe(object listener);
}

public class ListenersCollection: IListenersCollection, IEnumerable<object>
{
	public readonly Dictionary<Type, HashSet<object>> _listeners = new Dictionary<Type, HashSet<object>>();

	public void RegisterType<T>()
	{
		if (!_listeners.ContainsKey(typeof(T)))
			_listeners.Add(typeof(T), new HashSet<object>());
	}

	public void UnregisterType<T>()
	{
		HashSet<object> list;
		if (!_listeners.TryGetValue(typeof(T), out list))
			return;

		list.Clear();
		_listeners.Remove(typeof(T));
	}

	public void InvokeListeners<T>(Action<T> action)
	{
		HashSet<object> list;
		if (!_listeners.TryGetValue(typeof(T), out list))
		{
			list = new HashSet<object>();
			_listeners.Add(typeof(T), list);
		}

		foreach (var listener in list.ToArray())
			action((T) listener);
	}

	public void Subscribe(object listener)
	{
		var listenerType = listener.GetType();
		foreach (var type in _listeners.Keys)
		{
			if (!type.IsAssignableFrom(listenerType))
				continue;

			_listeners[type].Add(listener);
		}
	}

	public void Unsubscribe(object listener)
	{
		var listenerType = listener.GetType();
		foreach (var type in _listeners.Keys)
		{
			if (!type.IsAssignableFrom(listenerType))
				continue;

			_listeners[type].Remove(listener);
		}
	}

	public void UnsubscribeAll()
	{
		foreach (var list in _listeners.Values)
			list.Clear();
	}

	public IEnumerator<object> GetEnumerator()
	{
		foreach (var pair in _listeners)
		{
			foreach (var listener in pair.Value)
			{
				yield return listener;
			}
		}
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return GetEnumerator();
	}
}