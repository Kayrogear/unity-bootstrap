﻿
using System;
using System.Collections.Generic;
using UnityEngine;


public static class MessagesSystem
{
	public delegate void EventDelegate<T>(object sender, T msg) where T : MessageBase;
	private delegate void EventDelegate(object sender, MessageBase msg);

	public static bool DebugMessages { get; set; }

	private static Dictionary<Delegate, EventDelegate> _lookup = new Dictionary<Delegate, EventDelegate>();
	private static Dictionary<Type, List<EventDelegate>> _delegatesDict = new Dictionary<Type, List<EventDelegate>>();


	public static void AddListener<T>(EventDelegate<T> d)
		where T : MessageBase
	{
		if (_lookup.ContainsKey(d))
		{
#if DEBUG
			Debug.LogWarning($"Already contains {d}");
#endif
			return;
		}

		EventDelegate internalDelegate = (sender, message) => d(sender, (T)message);
		_lookup[d] = internalDelegate;

		Type t = typeof(T);
		if (!_delegatesDict.ContainsKey(t))
			_delegatesDict[t] = new List<EventDelegate>();

		var list = _delegatesDict[t];
		list.Add(internalDelegate);
	}


	public static void RemoveListener<T>(EventDelegate<T> d)
		where T : MessageBase
	{
		EventDelegate internalDelegate;
		if (!_lookup.TryGetValue(d, out internalDelegate))
			return;

		_lookup.Remove(d);
		Type t = typeof(T);
		var list = _delegatesDict[t];
		list.Remove(internalDelegate);

		if (list.Count == 0)
			_delegatesDict.Remove(t);
	}


	public static void RemoveAllListeners()
	{
		_lookup.Clear();
		_delegatesDict.Clear();
	}


	public static bool HasListener<T>(EventDelegate<T> d)
		where T : MessageBase
	{
		return _lookup.ContainsKey(d);
	}


	public static void Send(object sender, MessageBase msg)
	{
		if (DebugMessages)
			Debug.Log("- Send: " + msg);

		if (!_delegatesDict.ContainsKey(msg.GetType())) return;

		var delegatesArray = _delegatesDict[msg.GetType()].ToArray();
		foreach (var d in delegatesArray)
		{
			try
			{
				d.Invoke(sender, msg);
			}
			catch (Exception e)
			{
				Debug.LogException(e);
			}
		}
	}

	public static void SendDeferred(object sender, MessageBase msg)
	{
		if (DebugMessages)
			Debug.Log(" - Deferred send: " + msg);

		if (!_delegatesDict.ContainsKey(msg.GetType())) return;

		Dispatcher.Instance.Invoke(()=>
		{
			var delegatesArray = _delegatesDict[msg.GetType()].ToArray();
			foreach (var d in delegatesArray)
			{
				try
				{
					d.Invoke(sender, msg);
				}
				catch (Exception e)
				{
					Debug.LogException(e);
				}
			}
		});
	}
	
}
