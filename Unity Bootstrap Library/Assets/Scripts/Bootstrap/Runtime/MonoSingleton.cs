﻿
using UnityEngine;

public abstract class MonoSingleton<T> : MonoBehaviour
	where T : MonoSingleton<T>
{
	private static T _instance;

	public static bool IsAvailable => _instance != null;

	public static T Instance
	{
		get
		{
			if (_instance != null)
			{
				return _instance;
			}

			_instance = FindObjectOfType<T>();
			if (_instance != null)
			{
				return _instance;
			}

			var name = typeof(T).Name;
			var go = new GameObject("_" + name, typeof(T));
			_instance = go.GetComponent<T>();

			return _instance;
		}
	}

	protected virtual void Awake()
	{
		if (_instance == null)
		{
			_instance = this as T;
		}

		if (_instance != null && !ReferenceEquals(_instance, this))
		{
			Debug.LogErrorFormat($"Instance {GetType().Name} ({name}) already exist! ");
			DestroyImmediate(gameObject);
			return;
		}

		Debug.LogFormat($"Create Singleton {GetType().Name} ({name})");
		OnSingletonInit();
	}

	protected virtual void OnDestroy()
	{
		OnSingletonDestroy();
		_instance = null;
		Debug.LogFormat($"Create Singleton {GetType().Name} ({name})");
	}

	protected virtual void OnSingletonInit()
	{

	}

	protected virtual void OnSingletonDestroy()
	{

	}
}
