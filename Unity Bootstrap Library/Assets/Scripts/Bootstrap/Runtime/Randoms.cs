﻿using System;


public interface IRandomProvider
{
    int Next(int max);
    int Next(int min, int max);
}

public class ThreadSafeRandom : IRandomProvider
{
    private readonly Random _rnd;

    public ThreadSafeRandom(int seed = 0)
    {
        _rnd = new Random(seed == 0 ? (int)DateTime.Now.Ticks : seed);
    }

    public int Next(int max)
    {
        lock (_rnd)
            return _rnd.Next(max);
    }

    public int Next(int min, int max)
    {
        lock (_rnd)
            return _rnd.Next(min, max);
    }
}

public class WrappedRandom : IRandomProvider
{
    private readonly Random _rnd;

    public WrappedRandom(int seed = 0)
    {
        _rnd = new Random(seed == 0 ? (int)DateTime.Now.Ticks : seed);
    }

    public int Next(int max)
    {
        return _rnd.Next(max);
    }

    public int Next(int min, int max)
    {
        return _rnd.Next(min, max);
    }
}
