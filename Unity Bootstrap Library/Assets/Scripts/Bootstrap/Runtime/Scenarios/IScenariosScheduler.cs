﻿
using System.Collections;

public interface IScenariosScheduler
{
	Scenario Run(IEnumerator coroutine);

	void Start(Scenario scenario);
	void Stop(Scenario scenario);
	void CancelAll();
}
