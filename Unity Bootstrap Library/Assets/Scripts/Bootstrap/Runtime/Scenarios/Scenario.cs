﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface IReadonlyScenario : IEnumerator
{
	ScenarioState State { get; }
	Exception Exception { get; }

	bool IsRun { get; }
	bool IsFinished { get; }
	bool IsCancel { get; }
	bool IsFaulted { get; }
	bool IsCompletedOk { get; }
}

public class Scenario: CustomYieldInstruction, IReadonlyScenario
{
	public event Action<ScenarioState> StateChanged;
	public ScenarioState State => _state;
	public Exception Exception { get; private set; }

	public bool IsRun => State == ScenarioState.Running || State == ScenarioState.WaitingToRun;
	public bool IsFinished =>
		State == ScenarioState.RunToCompletion 
		|| State == ScenarioState.Canceled 
		|| State == ScenarioState.Faulted;
	public bool IsCancel => State == ScenarioState.Canceled;
	public bool IsFaulted => State == ScenarioState.Faulted;
	public bool IsCompletedOk => State == ScenarioState.RunToCompletion;

	public override bool keepWaiting => !IsFinished;

	protected object Current => _coroutine.Current;

	private readonly IScenariosScheduler _scheduler;
	private readonly IEnumerator _coroutine;
	private ScenarioState _state;

	private readonly string _debugName;
	private bool _keepWaiting;

	public Scenario(IScenariosScheduler scheduler, IEnumerator coroutine, bool run = false)
	{
		_scheduler = scheduler;
		_coroutine = coroutine;
		_debugName = _coroutine.GetType().Name;

		if (run) Run();
	}

	public void Run()
	{
		if (_state != ScenarioState.Created)
			throw new ScenarioException("Invalid scenario state!");

		ChangeState(ScenarioState.WaitingToRun);
		_scheduler.Start(this);
	}

	public void Cancel()
	{
		if (!IsRun) return;
		Stop(ScenarioState.Canceled);
	}

	protected virtual void OnComplete() { }
	protected virtual void OnCanceled() { }
	protected virtual void OnFaulted() { }
	
	private void Stop(Exception exception)
	{
		Exception = exception;
		Stop(ScenarioState.Faulted);
	}

	private void Stop(ScenarioState state)
	{
		if (!IsRun) return;
		if (state != ScenarioState.RunToCompletion
			&& state != ScenarioState.Canceled
			&& state != ScenarioState.Faulted) return;

		ChangeState(state);

		switch (state)
		{
			case ScenarioState.RunToCompletion:
				OnComplete();
				break;
			case ScenarioState.Canceled:
				OnCanceled();
				break;
			case ScenarioState.Faulted:
				OnFaulted();
				break;
			default:
				throw new ArgumentOutOfRangeException("state", state, null);
		}

		_scheduler.Stop(this);

		
	}

	private void ChangeState(ScenarioState state)
	{
		if (_state >= state) return;
		_state = state;
		StateChanged?.Invoke(_state);
	}

	internal IEnumerator InternalCoroutine()
	{
		if (_state != ScenarioState.WaitingToRun)
		{
			Stop(new ScenarioException("Invalid scenario state!"));
			yield break;
		}

		ChangeState(ScenarioState.Running);

		var stack = new Stack<IEnumerator>();
		stack.Push(_coroutine);

		while (State == ScenarioState.Running)
		{
			var coroutine = stack.Peek();
			try
			{
				if (!coroutine.MoveNext())
				{
					stack.Pop();
					if (stack.Count != 0) continue;
					break;
				}
			}
			catch (Exception e)
			{
				// exit point
				Debug.LogException(e);
				Stop(e);
				yield break;
			}

			if (coroutine.Current is Scenario scenario)
			{
				if (scenario == this)
				{
					// exit point
					Stop(new ScenarioException("Inner circular enumeration!"));
					yield break;
				}

				// wait inner scenario
				while (true)
				{
					if (scenario.IsFinished) break;
					yield return scenario;
				}
			}
			else if (coroutine.Current is IEnumerator enumerator)
			{
				stack.Push(enumerator);
				yield return enumerator;
			}
			else
			{
				yield return coroutine.Current;
			}
		}

		Stop(ScenarioState.RunToCompletion);
	}	
}


public class Scenario<TResult> : Scenario
{
	public TResult Result { get; private set; }

	public Scenario(IScenariosScheduler scheduler, IEnumerator coroutine, bool run = false) 
		: base(scheduler, coroutine, run)
	{
		Result = default(TResult);
	}

	protected override void OnComplete()
	{
		Result = (TResult)Current;
	}
}