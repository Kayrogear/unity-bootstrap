﻿using System;

public class ScenarioException : Exception
{
	public ScenarioException(string message) : base(message)
	{
	}
}