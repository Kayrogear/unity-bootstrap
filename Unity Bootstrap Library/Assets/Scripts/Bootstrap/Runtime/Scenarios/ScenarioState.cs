﻿
public enum ScenarioState
{
	Created,
	WaitingToRun,
	Running,
	RunToCompletion,
	Canceled,
	Faulted
}
