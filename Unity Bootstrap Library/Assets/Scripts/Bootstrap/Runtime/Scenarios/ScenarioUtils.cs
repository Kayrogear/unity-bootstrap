﻿
using System.Collections;
using System.Linq;
using UnityEngine;

public static class ScenarioUtils
{
	public static Scenario StartScenario(this MonoBehaviour behaviour, IEnumerator scenario)
	{
		var scheduler = behaviour.GetComponent<SchedulerBehaviour>();
		if (scheduler == null)
			scheduler = behaviour.gameObject.AddComponent<SchedulerBehaviour>();

		return scheduler.Run(behaviour, scenario);
	}

	public static void StopScenario(this MonoBehaviour behaviour, Scenario scenario)
	{
		var scheduler = behaviour.GetComponent<SchedulerBehaviour>();
		if (scheduler == null)
			scheduler = behaviour.gameObject.AddComponent<SchedulerBehaviour>();

		scheduler.Stop(behaviour, scenario);
	}

	public static void StopAllScenarios(this MonoBehaviour behaviour)
	{
		var scheduler = behaviour.GetComponent<SchedulerBehaviour>();
		if (scheduler == null)
			scheduler = behaviour.gameObject.AddComponent<SchedulerBehaviour>();

		scheduler.CancelAll(behaviour);
	}
}