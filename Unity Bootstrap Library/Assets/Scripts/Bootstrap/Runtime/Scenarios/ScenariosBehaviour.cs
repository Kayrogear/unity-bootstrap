﻿public class ScenariosBehaviour : MonoSingleton<ScenariosBehaviour>
{
	public IScenariosScheduler Scheduler => _scheduler;

	private ScenariosScheduler _scheduler;

	protected override void OnSingletonInit()
	{
		_scheduler = new ScenariosScheduler();
	}

	private void Update()
	{
		_scheduler.Process();
	}
}