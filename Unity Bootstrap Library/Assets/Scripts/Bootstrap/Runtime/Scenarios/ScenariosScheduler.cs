﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ScenariosScheduler : IScenariosScheduler
{
	private readonly Dictionary<Scenario, IEnumerator> _scenarios = new Dictionary<Scenario, IEnumerator>();

	public Scenario<TResult> Run<TResult>(IEnumerator coroutine)
	{
		var scenario = new Scenario<TResult>(this, coroutine);
		scenario.Run();
		return scenario;
	}

	public Scenario Run(IEnumerator coroutine)
	{
		var scenario = new Scenario(this, coroutine);
		scenario.Run();
		return scenario;
	}

	public void CancelAll()
	{
		while (_scenarios.Any())
			_scenarios.First().Key.Cancel();
	}

	public void Start(Scenario scenario)
	{
		if (scenario.State != ScenarioState.WaitingToRun) return;
		if (_scenarios.ContainsKey(scenario)) return;

		_scenarios.Add(scenario, scenario.InternalCoroutine());
	}

	public void Stop(Scenario scenario)
	{
		if (!scenario.IsFinished) return;
		_scenarios.Remove(scenario);
	}
	
	public void Process()
	{
		var scenarios = _scenarios.ToArray();
		foreach (var pair in scenarios)
			pair.Value.MoveNext();
	}
}