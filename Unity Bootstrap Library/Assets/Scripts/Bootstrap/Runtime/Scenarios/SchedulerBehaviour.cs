﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[DisallowMultipleComponent]
public class SchedulerBehaviour : MonoBehaviour
{
	private Dictionary<MonoBehaviour, ScenariosScheduler> _schedulers = new Dictionary<MonoBehaviour, ScenariosScheduler>();
	//private List<ScenariosScheduler> _temp = new List<ScenariosScheduler>();

	private void Awake()
	{
		hideFlags = HideFlags.HideAndDontSave | HideFlags.HideInInspector;
	}
	
	private void Update()
	{
		var toRemove = _schedulers.Keys.Where(k => k == null);
		if (toRemove.Any())
			foreach (var behaviour in toRemove.ToArray())
			{
				_schedulers[behaviour].CancelAll();
				_schedulers.Remove(behaviour);
			}
			
		foreach (var pair in _schedulers)
		{
			if (pair.Key.isActiveAndEnabled)
				pair.Value.Process();
		}
	}

	private void OnDestroy()
	{
		foreach (var pair in _schedulers)
			pair.Value.CancelAll();
		_schedulers.Clear();
	}

	public Scenario Run(MonoBehaviour behaviour, IEnumerator coroutine)
	{
		if (behaviour.gameObject != gameObject) return null;
		return GetScheduler(behaviour).Run(coroutine);
	}

	public void Stop(MonoBehaviour behaviour, Scenario scenario)
	{
		if (behaviour.gameObject != gameObject) return;
		GetScheduler(behaviour).Stop(scenario);
	}

	public void CancelAll(MonoBehaviour behaviour)
	{
		if (behaviour.gameObject != gameObject) return;
		GetScheduler(behaviour).CancelAll();
		_schedulers.Clear();
	}

	private ScenariosScheduler GetScheduler(MonoBehaviour behaviour)
	{
		if (!_schedulers.TryGetValue(behaviour, out var scheduler))
		{
			scheduler = new ScenariosScheduler();
			_schedulers[behaviour] = scheduler;
		}

		return scheduler;
	}
}