﻿using UnityEngine;

public abstract class ScriptableSingleton<T> : ScriptableObject
	where T : ScriptableSingleton<T>
{
	private static T _instance;

	public static T Instance
	{
		get
		{
			if (_instance != null)
				return _instance;

			var name = typeof(T).Name;
			_instance = Resources.Load<T>(name);

			if (Application.isPlaying)
				Debug.LogError($"Load SO Singleton {_instance}");	
			else
				Debug.Log($"Editor Load SO Singleton {_instance}");	

			_instance?.OnSingletonInit();

			return _instance;
		}
	}

	public static ResourceRequest AsyncInitialize()
	{
		var name = typeof(T).Name;
		var resourceRequest = Resources.LoadAsync<T>(name);

		resourceRequest.completed += (operation) =>
		{
			_instance = (T) resourceRequest.asset;

			Debug.Log($"Async Load SO Singleton {_instance}");
			_instance?.OnSingletonInit();

		};
		return resourceRequest;
	}

	protected virtual void OnDestroy()
	{
		OnSingletonDestroy();
		_instance = null;
		Debug.LogFormat("Destroy SO Singleton {0}", this);
	}


	protected virtual void OnSingletonInit()
	{

	}

	protected virtual void OnSingletonDestroy()
	{

	}
}