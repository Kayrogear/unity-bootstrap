﻿using System;
using System.Collections.Generic;

public enum SwitchStateResult
{
	Success = 0,
	TransitionNotExist,
	TransitionIsLocked,
	AlreadyInTransition
}

public interface IInSyntax<TStateEnum>
{
    TStateEnum Id { get; }
    IGotoSyntax<TStateEnum> Goto(TStateEnum stateId);
    IInSyntax<TStateEnum> ExecuteEnter(Action action);
    IInSyntax<TStateEnum> ExecuteEnter(Action<TStateEnum> action);
    IInSyntax<TStateEnum> ExecuteLeave(Action action);
    IInSyntax<TStateEnum> ExecuteLeave(Action<TStateEnum> action);
}

public interface IGotoSyntax<TStateEnum> : IInSyntax<TStateEnum>
{
    IGotoSyntax<TStateEnum> Execute(Action action);
    IGotoSyntax<TStateEnum> Execute(Action<TStateEnum, TStateEnum> action);
    IGotoSyntax<TStateEnum> IfGuard(Func<bool> action);
    IGotoSyntax<TStateEnum> IfGuard(Func<TStateEnum, TStateEnum, bool> action);
}

public class StateMachine<TStateEnum> where TStateEnum : struct
{
	public delegate void StateChageHandler(TStateEnum fromState, TStateEnum toState);

    private readonly Dictionary<TStateEnum, State> _statesDict = new Dictionary<TStateEnum, State>();
    private State _currentState;
    private bool _switchGuard;

    public TStateEnum? CurrentStateId
    {
        get => _currentState?.Id;
		set
        {
            if (value.HasValue)
                SwitchState(value.Value);
        }
    }

    public event StateChageHandler StateChangeBegin;
    public event StateChageHandler StateChangeEnd;

    public void Start(TStateEnum stateId)
    {
        CurrentStateId = stateId;
    }

    public IInSyntax<TStateEnum> In(TStateEnum stateId)
    {
        State state;
        if (!_statesDict.TryGetValue(stateId, out state))
        {
            state = new State(this, stateId);
            _statesDict[stateId] = state;
        }
        return state;
    }

	public SwitchStateResult CanSwitchTo(TStateEnum stateId)
	{
		var fromState = CurrentStateId ?? default(TStateEnum);
		var toState = stateId;

		if (_switchGuard) return SwitchStateResult.AlreadyInTransition;

		if (_currentState != null)
		{
			var trans = _currentState.GetTransition(stateId);
			
			// transition does't exists
			if (trans == null)
				return SwitchStateResult.TransitionNotExist;
			
			// transition is locked
			if (!trans.InvokeGuard(_currentState.Id))
				return SwitchStateResult.TransitionIsLocked;

		}

		return SwitchStateResult.Success;
	}

    public SwitchStateResult SwitchState(TStateEnum stateId)
    {
        Transition trans = null;

        var fromState = CurrentStateId ?? default(TStateEnum);
        var toState = stateId;

        // locking switch in Leave & Transition Actions
        if (_switchGuard) return SwitchStateResult.AlreadyInTransition;
        _switchGuard = true;
        {
            OnStateChangeBegin(fromState, toState);

            if (_currentState != null)
            {
                trans = _currentState.GetTransition(stateId);

                // transition does't exists
                if (trans == null)
                {
                    _switchGuard = false;
					return SwitchStateResult.TransitionNotExist;
					//throw new StateMachineException(string.Format("Transition {0}->{1} does't exists", fromState, toState));
				}

                // transition is locked
                if (!trans.InvokeGuard(_currentState.Id))
                {
                    _switchGuard = false;
					return SwitchStateResult.TransitionIsLocked;
				}

                // execute leave current state
                _currentState.InvokeLeave();
            }

            // execute transition action
            if (trans != null)
                trans.Invoke(_currentState.Id);

            _currentState = (State) In(stateId);
        }
        _switchGuard = false;

        OnStateChangeEnd(fromState, toState);

        // execute enter new state (switch is allowed)
        _currentState.InvokeEnter();

		return SwitchStateResult.Success;
	}

    protected virtual void OnStateChangeBegin(TStateEnum arg1, TStateEnum arg2)
    {
        if (StateChangeBegin != null)
            StateChangeBegin.Invoke(arg1, arg2);
    }

    protected virtual void OnStateChangeEnd(TStateEnum arg1, TStateEnum arg2)
    {
        if (StateChangeEnd != null)
            StateChangeEnd.Invoke(arg1, arg2);
    }

    internal class Transition : IGotoSyntax<TStateEnum>
    {
        private readonly State _state;
        private Func<bool> _guard;
        private Func<TStateEnum, TStateEnum, bool> _guardFromTo;
        private Action _trans;
        private Action<TStateEnum, TStateEnum> _transFromTo;

        public Transition(State state, TStateEnum toId)
        {
            _state = state;
            ToId = toId;
        }

        public TStateEnum ToId { get; private set; }

        public TStateEnum Id
        {
            get { return _state.Id; }
        }

        public IGotoSyntax<TStateEnum> Goto(TStateEnum stateId)
        {
            return _state.Goto(stateId);
        }

        public IInSyntax<TStateEnum> ExecuteEnter(Action action)
        {
            return _state.ExecuteEnter(action);
        }

        public IInSyntax<TStateEnum> ExecuteEnter(Action<TStateEnum> action)
        {
            return _state.ExecuteEnter(action);
        }

        public IInSyntax<TStateEnum> ExecuteLeave(Action action)
        {
            return _state.ExecuteLeave(action);
        }

        public IInSyntax<TStateEnum> ExecuteLeave(Action<TStateEnum> action)
        {
            return _state.ExecuteLeave(action);
        }

        public IGotoSyntax<TStateEnum> Execute(Action action)
        {
            _trans = action;
            _transFromTo = null;
            return this;
        }

        public IGotoSyntax<TStateEnum> Execute(Action<TStateEnum, TStateEnum> action)
        {
            _trans = null;
            _transFromTo = action;
            return this;
        }

        public IGotoSyntax<TStateEnum> IfGuard(Func<bool> action)
        {
            _guard = action;
            _guardFromTo = null;
            return this;
        }

        public IGotoSyntax<TStateEnum> IfGuard(Func<TStateEnum, TStateEnum, bool> action)
        {
            _guard = null;
            _guardFromTo = action;
            return this;
        }

        public bool Invoke(TStateEnum from)
        {
            if (_trans != null)
            {
                _trans();
                return true;
            }
            if (_transFromTo != null)
            {
                _transFromTo(@from, ToId);
                return true;
            }

            return false;
        }

        public bool InvokeGuard(TStateEnum from)
        {
            if (_guard != null)
                return _guard();

            if (_guardFromTo != null)
                return _guardFromTo(from, ToId);

            return true;
        }
    }

    internal class State : IInSyntax<TStateEnum>
    {
        private readonly StateMachine<TStateEnum> _machine;
        private readonly Dictionary<TStateEnum, Transition> _transDict = new Dictionary<TStateEnum, Transition>();
        private Action _enter;
        private Action<TStateEnum> _enterTo;
        private Action _leave;
        private Action<TStateEnum> _leaveFrom;

        public State(StateMachine<TStateEnum> machine, TStateEnum id)
        {
            _machine = machine;
            Id = id;
        }

        public TStateEnum Id { get; private set; }

        public IGotoSyntax<TStateEnum> Goto(TStateEnum stateId)
        {
            Transition transition;
            if (!_transDict.TryGetValue(stateId, out transition))
            {
                var stateTo = _machine.In(stateId);
                transition = new Transition(this, stateTo.Id);
                _transDict[stateId] = transition;
            }

            return transition;
        }

        public IInSyntax<TStateEnum> ExecuteEnter(Action action)
        {
            _enter = action;
            _enterTo = null;
            return this;
        }

        public IInSyntax<TStateEnum> ExecuteEnter(Action<TStateEnum> action)
        {
            _enter = null;
            _enterTo = action;
            return this;
        }

        public IInSyntax<TStateEnum> ExecuteLeave(Action action)
        {
            _leave = action;
            _leaveFrom = null;
            return this;
        }

        public IInSyntax<TStateEnum> ExecuteLeave(Action<TStateEnum> action)
        {
            _leave = null;
            _leaveFrom = action;
            return this;
        }

        public Transition GetTransition(TStateEnum stateId)
        {
            Transition trans;
            _transDict.TryGetValue(stateId, out trans);
            return trans;
        }

        public bool InvokeEnter()
        {
            if (_enter != null)
            {
                _enter();
                return true;
            }

            if (_enterTo != null)
            {
                _enterTo(Id);
                return true;
            }

            return false;
        }

        public bool InvokeLeave()
        {
            if (_leave != null)
            {
                _leave();
                return true;
            }

            if (_leaveFrom != null)
            {
                _leaveFrom(Id);
                return true;
            }

            return false;
        }
    }
}
