﻿using System;

public class StateMachineException : Exception
{
	public StateMachineException(string message) : base(message)
	{
	}
}