﻿
using System;
using System.Collections.Generic;
using System.Linq;
using Random = UnityEngine.Random;

public static class EnumUtils
{
	public static T RandomValue<T>()
		where T : struct, IFormattable, IConvertible, IComparable
	{
		var values = Enum.GetValues(typeof(T)).OfType<T>().ToList();
		var index = Random.Range(0, values.Count);

		return values[index];
	}

	public static T RandomExclude<T>(params T[] exclude)
		where T : struct, IFormattable, IConvertible, IComparable
	{
		var values = Enum.GetValues(typeof(T))
			.OfType<T>()
			.Where(o=> !exclude.Contains(o))
			.ToList();

		var index = Random.Range(0, values.Count);

		return values[index];
	}

	public static bool IsDefined<T>(object value)
		where T : struct, IFormattable, IConvertible, IComparable
	{
		return Enum.IsDefined(typeof(T), value);
	}


	public static T Parse<T>(string value, bool ignoreCase = false)
		where T : struct, IFormattable, IConvertible, IComparable
	{
		return (T)Enum.Parse(typeof(T), value, ignoreCase);
	}


	public static bool TryParse<T>(string value, out T result, bool ignoreCase = false)
		where T : struct, IFormattable, IConvertible, IComparable
	{

		try
		{
			result = (T) Enum.Parse(typeof(T), value, ignoreCase);
		}
		catch (Exception)
		{
			result = default(T);
			return false;
		}
		return true;
	}


	public static T? TryParse<T>(string value, bool ignoreCase = false)
		where T : struct, IFormattable, IConvertible, IComparable
	{
		try { return (T)Enum.Parse(typeof(T), value, ignoreCase); }
		catch (Exception) { }
		return null;
	}


	public static T[] Values<T>()
		where T : struct, IFormattable, IConvertible, IComparable
	{
		return (T[])Enum.GetValues(typeof(T));
	}

	public static T[] ValuesExclude<T>(params T[] exclude)
		where T : struct, IFormattable, IConvertible, IComparable
	{
		var values = Enum.GetValues(typeof(T))
			.OfType<T>()
			.Where(o => !exclude.Contains(o));

		return values.ToArray();
	}

	public static List<T> ToFlagsList<T>(T value) 
		where T : struct, IFormattable, IConvertible, IComparable
	{
		var result = new List<T>();
		var intValue = Convert.ToInt32(value);
		foreach (int enumValue in Enum.GetValues(typeof(T)))
		{
			if ((intValue & enumValue) == enumValue)
			{
				var v = (T) Enum.ToObject(typeof(T), enumValue);
				result.Add(v);
			}
		}
		return result;
	} 
}
