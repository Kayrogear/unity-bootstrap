﻿using UnityEngine;

public static class RectTransformUtils
{
	//public static Rect GetWorldRect(this RectTransform rt)
	//{
	//	return GetWorldRect(rt, Vector2.one);
	//}

	public static Rect GetWorldRect(this RectTransform rt)
	{
		Vector3[] corners = new Vector3[4];

		rt.GetWorldCorners(corners);

		Vector3 topLeft = Camera.main.WorldToScreenPoint(corners[0]);
		Vector3 rightBottom = Camera.main.WorldToScreenPoint(corners[2]);

		return new Rect(topLeft, rightBottom - topLeft);
	}
}