﻿using System;
using System.Collections;
using System.Threading.Tasks;

public static class TaskExtensions
{
	public static IEnumerator AsIEnumerator(this Task task)
	{
		while (!task.IsCompleted)
		{
			yield return null;
		}
 
		if (task.IsFaulted)
		{
			throw task.Exception;
		}
	}

	public static IEnumerator AsIEnumerator(this Task task, TimeSpan timeout)
	{
		var time = DateTime.Now;
		while (!task.IsCompleted && DateTime.Now - time < timeout)
		{
			yield return null;
		}
 
		if (task.IsFaulted)
		{
			throw task.Exception;
		}
	}
}