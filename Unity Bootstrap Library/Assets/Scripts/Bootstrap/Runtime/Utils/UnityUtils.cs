﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public static class UnityUtils
{
	public static Version GetAppVersion()
	{
		return Version.Parse(Application.version);
	}

	public static Rect Union(this Rect rectA, Rect rectB)
	{
		var xMin = Mathf.Min(rectA.min.x, rectB.min.x);
		var yMin = Mathf.Min(rectA.min.y, rectB.min.y);
		var xMax = Mathf.Max(rectA.max.x, rectB.max.x);
		var yMax = Mathf.Max(rectA.max.y, rectB.max.y);

		return new Rect() { xMin = xMin, yMin = yMin, xMax = xMax, yMax = yMax };
	}

	private static Random rng = new Random();  

	public static void Shuffle<T>(this IList<T> list)  
	{  
		int n = list.Count;  
		while (n > 1) {  
			n--;  
			int k = rng.Next(n + 1);  
			T value = list[k];  
			list[k] = list[n];  
			list[n] = value;  
		}  
	}
}