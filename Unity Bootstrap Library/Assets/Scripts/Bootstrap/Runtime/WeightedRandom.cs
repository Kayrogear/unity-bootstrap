﻿using System;
using System.Collections.Generic;
using System.Linq;

public class WeightedRandom<T>
{
    public IEnumerable<T> Available { get { return _weights.Select(p => p.Item1); } }

    private readonly Tuple<T, int>[] _weights;
    private readonly int _weightsSum;
    private readonly IRandomProvider _randomProvider;

    public WeightedRandom(IEnumerable<Tuple<T, int>> pairs, int seed = 0)
    {
        _weights = pairs.Where(p => p.Item2 != 0).Select(p => p).ToArray();
        _weightsSum = _weights.Sum(p => p.Item2);

        _randomProvider = new WrappedRandom(seed == 0 ? (int)DateTime.Now.Ticks : seed);
    }

    public WeightedRandom(IEnumerable<Tuple<T, int>> pairs, IRandomProvider randomProvider)
    {
        _randomProvider = randomProvider;
        _weights = pairs.Where(p => p.Item2 != 0).Select(p => p).ToArray();
        _weightsSum = _weights.Sum(p => p.Item2);
        _randomProvider = randomProvider;
    }

    public T GetRandom()
    {
        var value = _randomProvider.Next(0, _weightsSum + 1);
        var min = 0;
        foreach (var pair in _weights)
        {
            var max = min + pair.Item2;
            if (value >= min && value <= max)
                return pair.Item1;
            min = max;
        }
        return default(T);
    }
}
