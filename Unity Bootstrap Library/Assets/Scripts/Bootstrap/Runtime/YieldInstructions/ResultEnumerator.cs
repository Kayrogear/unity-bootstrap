﻿using System.Collections;

public class ResultEnumerator<TResult> : IEnumerator
{
	public object Current { get { return _coroutine.Current; } }
	public bool IsFinished { get { return _isFinished; }}
	public TResult Result { get { return _result; } }

	private readonly IEnumerator _coroutine;
	private bool _isFinished;
	private TResult _result;

	public ResultEnumerator(IEnumerator coroutine)
	{
		_coroutine = coroutine;
		_result = default(TResult);
		_isFinished = false;
	}

	public bool MoveNext()
	{
		var continueFlag = _coroutine.MoveNext();
		if (!continueFlag)
		{
			_result = (TResult) Current;
			_isFinished = true;
		}
		return continueFlag;
	}

	public void Reset()
	{
		_isFinished = false;
		_result = default(TResult);
		_coroutine.Reset();
	}

	
}

