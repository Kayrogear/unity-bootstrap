﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WaitAll : CustomYieldInstruction
{
	public override bool keepWaiting { get { return _yields.Any(y => y != null && y.keepWaiting); } }

	private readonly IEnumerable<CustomYieldInstruction> _yields;
	
	public WaitAll(params CustomYieldInstruction[] yields)
	{
		_yields = yields;
	}

	public WaitAll(IEnumerable<CustomYieldInstruction> yields)
	{
		_yields = yields;
	}

	public WaitAll(IEnumerable<IReadonlyScenario> scenarios)
	{
		_yields = scenarios.Cast<CustomYieldInstruction>();
	}

	
}