﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WaitAllAsync : CustomYieldInstruction
{
	public override bool keepWaiting { get { return _operations.Any(y => y != null && !y.isDone); } }

	private readonly IEnumerable<AsyncOperation> _operations;

	public WaitAllAsync(IEnumerable<AsyncOperation> operations)
	{
		_operations = operations;
	}

	public WaitAllAsync(params AsyncOperation[] operations)
	{
		_operations = operations;
	}
}