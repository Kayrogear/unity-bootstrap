﻿using UnityEngine;

public class WaitForGameSeconds : CustomYieldInstruction
{
	public override bool keepWaiting => Time.time - _ts < _seconds;

	private float _seconds;
	private readonly float _ts;

	public WaitForGameSeconds(float seconds)
	{
		_seconds = seconds;
		_ts = Time.time;
	}
}