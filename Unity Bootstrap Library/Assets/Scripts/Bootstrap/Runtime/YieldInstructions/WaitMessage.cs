﻿using System;
using UnityEngine;

public class WaitMessage<TMessage> : CustomYieldInstruction where TMessage: MessageBase
{
	public override bool keepWaiting => _keepWaiting;

	private readonly Func<TMessage, bool> _predicate;
	private bool _keepWaiting = true;

	public WaitMessage()
		: this(null)
	{

	}

	public WaitMessage(Func<TMessage, bool> predicate)
	{
		_predicate = predicate;
		MessagesSystem.AddListener<TMessage>(OnMessageIncoming);
	}

	private void OnMessageIncoming(object sender, TMessage msg)
	{
		if (_predicate != null && !_predicate(msg))
			return;

		MessagesSystem.RemoveListener<TMessage>(OnMessageIncoming);
		_keepWaiting = false;
	}
}

