﻿using System.Linq;
using UnityEngine;

public class WaitOne : CustomYieldInstruction
{
	public override bool keepWaiting { get { return _yields.All(y => y.keepWaiting); } }

	private readonly CustomYieldInstruction[] _yields;
	
	public WaitOne(params CustomYieldInstruction[] yields)
	{
		_yields = yields;
	}
}